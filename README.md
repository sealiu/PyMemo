# PyMemo
毕业设计——单词记忆软件

----

这是我的一项毕业设计，课题为：基于Python的单词记忆软件开发。

目前仍处于开发阶段！

----

Python: 2.7.9

GUI：wxpython2.7.9

IDE：PyCharm

DB: SQLite3

----

其中的图标大部分都来自：[http://findicons.com](http://findicons.com)

向所有提出过建议，报告bug的人表示感谢！

----

Email：iliuyang@foxmail.com

豆瓣小组讨论：[Python编程小组](http://www.douban.com/group/topic/72255077/)


